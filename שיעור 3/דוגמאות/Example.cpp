#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("temp.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	cout << "select * from customers" << endl;

	clearTable();

	rc = sqlite3_exec(db, "select * from customers", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");

	cout << "select * from customers where customerAge > 35" << endl;

	clearTable();

	rc = sqlite3_exec(db, "select * from customers where customerAge > 35", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");

	cout << "insert into customers (customerID, customerName, customerAge) values(18, 'Dinno', 45)" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into customers (customerID, customerName, customerAge) values(18, 'Dinno', 45)", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	cout << "select * from customers" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select * from customers", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");

	cout << "select * from customers LIMIT 5" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select * from customers LIMIT 5", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	cout << "select * from orders LIMIT 5" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select * from orders LIMIT 5", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");
	cout << "select * from customers join orders where customers.customerID = orders.customerID" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select * from customers join orders where customers.customerID = orders.customerID", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");
	cout << "select carModel, count(*) from orders group by carModel" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select carModel, count(*) from orders group by carModel", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");
	cout << "select carModel, count(*) from orders group by carModel having count(*) > 2" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select carModel, count(*) from orders group by carModel having count(*) > 2", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");
	cout << "select carID, count(*) from cars group by carModel" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select carID, count(*) from cars group by carModel", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");

	cout << "select carModel, avg(carPrice) from cars group by carModel" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select carModel, avg(carPrice) from cars group by carModel", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");
	cout << "select * from orders" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select * from orders", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	cout << endl << "select carModel, count(*) from (select * from orders) group by carModel" << endl;

	clearTable();
	rc = sqlite3_exec(db, "select carModel, count(*) from (select * from orders) group by carModel", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("pause");

	sqlite3_close(db);

}